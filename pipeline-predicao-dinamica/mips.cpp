#include "mips.hpp"

Mips::Mips()
{
  PC = 0;

  for(int i = 0; i < 32; i++)
    R[i] = 0;

  for(int i = 0; i < 32; i++)
    prediction[i] = 0;

  predictionActive = 0;
  countExecutedInstructions = 0;
  countInvalidInstructions = 0;
}

Mips::~Mips()
{

}

void Mips::fetch(struct Instruction instruction)
{
  instructionFetch = instruction;

  //o "fetch" é feito ao carregar todas as intruções para a memória

  if (predictionActive && instructionFetch.Opcode == BEQ) {
    if (prediction[PC + instructionFetch.Op3] == 1)
      PC += instructionFetch.Op3;
    else
      PC++;
  }
  else {
    PC++;
  }
}

void Mips::decode(struct Instruction instruction)
{
  instructionDecode = instruction;

  if (instructionDecode.Valid == 1) {
    if (instructionDecode.Opcode == B)
      instructionDecode.valueTemp1 = instructionDecode.Op1;
    else
    {
      instructionDecode.valueTemp2 = R[instructionDecode.Op2];

      if ((instructionDecode.Opcode == ADDI) || (instructionDecode.Opcode == SUBI) || (instructionDecode.Opcode == BEQ))
        instructionDecode.valueTemp3 = instructionDecode.Op3;
      else
        instructionDecode.valueTemp3 = R[instructionDecode.Op3];

      if (instructionDecode.Opcode == BEQ)
        instructionDecode.valueTemp1 = R[instructionDecode.Op1];
    }
  }
  else
  {
  }
}

void Mips::execute(struct Instruction instruction)
{
  instructionExecute = instruction;

  if (instructionExecute.Valid == 1) {

    //ADDI AND AND
    if ((instructionExecute.Opcode == ADDI) || (instructionExecute.Opcode == ADD))
      instructionExecute.valueTemp1 = instructionExecute.valueTemp2 + instructionExecute.valueTemp3;

    //SUBI AND SUB
    if ((instructionExecute.Opcode == SUBI) || (instructionExecute.Opcode == SUB))
      instructionExecute.valueTemp1 = instructionExecute.valueTemp2 - instructionExecute.valueTemp3;

    //BEQ
    if (predictionActive)
    {
      //BEQ WITH PREDICTION
      if (instructionExecute.Opcode == BEQ) {
        if (instructionExecute.valueTemp1 == instructionExecute.valueTemp2) {
          prediction[PC + instructionExecute.valueTemp3 - 2] = 1;
          instructionDecode.Valid = 0;
          instructionFetch.Valid = 0;
        }
        else {
          prediction[PC + instructionExecute.valueTemp3 - 2] = 0;
        }

      }
    }
    else
    {
      //BEQ WITHOUT PREDICTION
      if (instructionExecute.Opcode == BEQ) {
        if (instructionExecute.valueTemp1 == instructionExecute.valueTemp2) {
          PC += instructionExecute.valueTemp3 - 2;
          instructionDecode.Valid = 0;
          instructionFetch.Valid = 0;
        }
      }
    }

    //B
    if (instructionExecute.Opcode == B){
      PC += instructionExecute.valueTemp1 - 2;
      // instructionDecode.Valid = 0;
      // instructionFetch.Valid = 0;
    }

  }

}

void Mips::memoryAccess(struct Instruction instruction)
{
  //SW E LW
  instructionMemoryAccess = instruction;
}

void Mips::writeBack(struct Instruction instruction)
{
  instructionWriteBack = instruction;

  if (instructionWriteBack.Valid == 1) {
    if ((instructionWriteBack.Opcode == ADDI) ||
        (instructionWriteBack.Opcode == ADD)  ||
        (instructionWriteBack.Opcode == SUBI) ||
        (instructionWriteBack.Opcode == SUB))
    {
      R[instructionWriteBack.Op1] = instructionWriteBack.valueTemp1;
    }
    countExecutedInstructions++;
  }
  else {
    countInvalidInstructions++;
  }
}

void Mips::printRegisters()
{
  std::cout << "Registers ------------------------------------------------------------------------- \n\r";
  for(int i = 0; i< 8; i++)
      std::cout << " R" << i << " - " << R[i];
  std::cout << "\n\r";
  for(int i = 8; i< 16; i++)
      std::cout << " R" << i << " - " << R[i];
  std::cout << "\n\r";
  for(int i = 16; i< 24; i++)
      std::cout << " R" << i << " - " << R[i];
  std::cout << "\n\r";
  for(int i = 24; i< 32; i++)
      std::cout << " R" << i << " - " << R[i];
  std::cout << "\n\r";
  std::cout << "----------------------------------------------------------------------------------- \n\r";
}

void Mips::printPC()
{
  std::cout << "PC: " << PC << "\n\r";
}

void Mips::printPrediction()
{
  std::cout << "Prediction ------------------------------------------------------------------------ \n\r";
  for(int i = 0; i< 8; i++)
      std::cout << "Pred" << i << " - " << prediction[i];
  std::cout << "\n\r";
  for(int i = 8; i< 16; i++)
      std::cout << "Pred" << i << " - " << prediction[i];
  std::cout << "\n\r";
  for(int i = 16; i< 24; i++)
      std::cout << "Pred" << i << " - " << prediction[i];
  std::cout << "\n\r";
  for(int i = 24; i< 32; i++)
      std::cout << "Pred" << i << " - " << prediction[i];
  std::cout << "\n\r";
  std::cout << "----------------------------------------------------------------------------------- \n\r";
}

void Mips::printInstruction(struct Instruction ram[256])
{
  std::cout << "Next Instruction --------------------" << "\n\r";
  std::cout << "Opcode: " << ram[PC].Opcode << "\n\r";
  std::cout << "Op1: " << ram[PC].Op1 << "\n\r";
  std::cout << "Op2: " << ram[PC].Op2 << "\n\r";
  std::cout << "Op3: " << ram[PC].Op3 << "\n\r";
  std::cout << "-------------------------------------" << "\n\r";
  std::cout << std::endl;
}

void Mips::printInstructionStep(struct Instruction instruction)
{
  std::cout << "Opcode: " << instruction.Opcode
  << " Op1: " << instruction.Op1
  << " Op2: " << instruction.Op2
  << " Op3: " << instruction.Op3 << "\n\r";
}

void Mips::printEsthatistics()
{
  std::cout << "Count Executed Instructions: " << countExecutedInstructions << "\n\r";
  std::cout << "Count Invalid Instructions: " << countInvalidInstructions << "\n\r";

}
