#ifndef MIPS_H
#define MIPS_H
#include <iostream>
#include <cmath>
#include "instruction.hpp"

class Mips {

  public:

    Mips();
    ~Mips();

    //methods
    void fetch(struct Instruction instruction);
    void decode(struct Instruction instruction);
    void execute(struct Instruction instruction);
    void memoryAccess(struct Instruction instruction);
    void writeBack(struct Instruction instruction);

    void printRegisters();
    void printPC();
    void printPrediction();
    void printInstruction(struct Instruction ram[256]);
    void printInstructionStep(struct Instruction instruction);
    void printEsthatistics();
    
    //attributes
    unsigned int PC;
    int R[32];
    unsigned char prediction[32];
    Instruction instructionFetch;
    Instruction instructionDecode;
    Instruction instructionExecute;
    Instruction instructionMemoryAccess;
    Instruction instructionWriteBack;

    unsigned char predictionActive;

    unsigned int countExecutedInstructions;
    unsigned int countInvalidInstructions;
};

#endif
