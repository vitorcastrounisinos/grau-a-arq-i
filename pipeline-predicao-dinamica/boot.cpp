#include "boot.hpp"

Instruction LoadInstruction(std::string opcode, std::string op1, std::string op2, std::string op3)
{
  struct Instruction instruction;

  instruction.Opcode = Opcode::NOP;

  if(opcode.compare("addi") == 0)
      instruction.Opcode = Opcode::ADDI;
  if(opcode.compare("add") == 0)
      instruction.Opcode = Opcode::ADD;
  if(opcode.compare("subi") == 0)
      instruction.Opcode = Opcode::SUBI;
  if(opcode.compare("sub") == 0)
      instruction.Opcode = Opcode::SUB;
  if(opcode.compare("b") == 0)
      instruction.Opcode = Opcode::B;
  if(opcode.compare("beq") == 0)
      instruction.Opcode = Opcode::BEQ;

  if(instruction.Opcode != Opcode::NOP && instruction.Opcode != Opcode::B) {
    instruction.Op1 = std::stoi(op1.substr(2, 1));
    instruction.Op2 = std::stoi(op2.substr(2, 1));
    if (instruction.Opcode == Opcode::ADD && instruction.Opcode != Opcode::SUB) {
      instruction.Op3 = std::stoi(op3.substr(2, 1));
    }
    else {
      instruction.Op3 = std::stoi(op3);
    }
  }
  else if(instruction.Opcode == Opcode::B) {
    instruction.Op1 = std::stoi(op1);
    instruction.Op2 = 0;
    instruction.Op3 = 0;
  }
  else {
    instruction.Op1 = 0;
    instruction.Op2 = 0;
    instruction.Op3 = 0;
  }

  instruction.Valid = 1;

  return instruction;
}

void LoadRAM(struct Instruction ram[256])
{
  std::string line;
  std::string opcode, op1, op2, op3;
  std::string fileName = "teste_exercicio.txt";
  std::ifstream file(fileName);

  if(file.is_open()) {
    std::cout << "Opened file: " << fileName << std::endl;
    int i = 0;
    do {
      file >> opcode;
      if(std::string(opcode) != "nop" && std::string(opcode) != "b")
        file >> op1 >> op2 >> op3;
      else
      {
        if (std::string(opcode) == "b")
          file >> op1;
        else
          op1 = "";

        op2 = "";
        op3 = "";
      }

      std::cout << "Readed: " << opcode << " " << op1 << " " << op2 << " " << op3 << " " << std::endl;
      ram[i] = LoadInstruction(opcode, op1, op2, op3);
      i++;
    } while (getline(file, line));
    file.close();
  }
  else
  {
    std::cout << "Error to open file: " << fileName << std::endl;
  }
}
