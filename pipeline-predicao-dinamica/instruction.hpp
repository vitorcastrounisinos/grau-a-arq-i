#ifndef INSTRUCTION_H
#define INSTRUCTION_H

enum Opcode {
  ADD = 1,
  ADDI = 2,
  SUB = 3,
  SUBI = 4,
  BEQ = 5,
  B = 6,
  NOP = 7
};

typedef struct Instruction {
  int Opcode = Opcode::NOP;
  int Op1 = 0;
  int Op2 = 0;
  int Op3 = 0;
  int Valid = 1;
  int valueTemp1 = 0;
  int valueTemp2 = 0;
  int valueTemp3 = 0;
} Instruction;

#endif
