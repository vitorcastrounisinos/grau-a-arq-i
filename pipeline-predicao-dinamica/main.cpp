#include <iostream>
#include "mips.hpp"
#include "boot.hpp"
#include "instruction.hpp"

int main() {

  int i = 0;

  //ARCHITECTURE
  Mips mips = Mips();
  Instruction ram[256];

  //BOOT
  std::cout << "Load code to RAM..." << std::endl;
  LoadRAM(ram);
  std::cout << std::endl;

  //PREDICTIVE OPTION
  std::cout << "Deseja usar predicao?" << std::endl;
  std::cout << "0 - Nao" << std::endl;
  std::cout << "1 - Sim" << std::endl;
  std::cout << "OBS: padrao eh usar sem predicao." << std::endl;

  std::cin >> i;

  if (i == 1)
    mips.predictionActive = 1;


  //PIPELINE CYCLE
  mips.printPC();
  mips.printInstruction(ram);
  mips.printRegisters();
  mips.printEsthatistics();

  while(std::cin.get()) {

    std::cout << "------------------------------------- Mips Cycle ---------------------------------------" << std::endl;

    mips.writeBack(mips.instructionMemoryAccess);
    std::cout << "WriteBack: ";
    mips.printInstructionStep(mips.instructionWriteBack);
    std::cout << std::endl;

    mips.memoryAccess(mips.instructionExecute);
    std::cout << "MemoryAccess: ";
    mips.printInstructionStep(mips.instructionMemoryAccess);
    std::cout << std::endl;

    mips.execute(mips.instructionDecode);
    std::cout << "Execute: ";
    mips.printInstructionStep(mips.instructionDecode);
    std::cout << std::endl;

    mips.decode(mips.instructionFetch);
    std::cout << "Decode: ";
    mips.printInstructionStep(mips.instructionDecode);
    std::cout << std::endl;

    mips.fetch(ram[mips.PC]);
    std::cout << "Fetch: ";
    mips.printInstructionStep(mips.instructionFetch);
    std::cout << std::endl;

    mips.printPC();
    mips.printInstruction(ram);
    mips.printRegisters();
    mips.printEsthatistics();

    std::cout << "----------------------------------------------------------------------------------------" << std::endl;
  }

  //SHUTDOWN


  return 0;
}
