#include <iostream>
#include <fstream>
#include <string>

#include "instruction.hpp"

Instruction LoadInstruction(std::string opcode, std::string op1, std::string op2, std::string op3);

void LoadRAM(struct Instruction ram[256]);
